import React from "react";
import Columns from "react-columns";
import axios from "axios";

class Sign extends React.Component {

  state = {
    match: "Your passwords dont match.",
    secure: "Your password is not secure.",
    color: "red",
    color02: "red",
    password: "",
    passwordMatch: "",
    length:false
  }

  componentWillMount(){

    const token = JSON.parse(localStorage.getItem("token"));
    if (token != null){ alert ("Already Logged in")
                      this.props.history.push("/")}
  }

  handleSubmitUp = (e) => {
    e.preventDefault()
    let email = e.target.elements.email.value;
    let name = e.target.elements.name.value;
    let lastName = e.target.elements.lastName.value;
    let adress = e.target.elements.adress.value;
    let cp = e.target.elements.CP.value;
    let city = e.target.elements.city.value;
    let province = e.target.elements.province.value;
    let state = e.target.elements.state.value;
    let password = e.target.elements.password.value;
    let passwordConfirm = e.target.elements.passwordConfirm.value;
    let newUser = { email, name, lastName, adress, cp, city, province, state, password, passwordConfirm };
    axios.post ("http://localhost:2000/user/add", newUser)
    .then ((res) =>{
      alert (res.data.message)
      localStorage.setItem("token",JSON.stringify(res.data.token))
      setTimeout( () => {
        this.props.checkLog();
        this.props.history.push({pathname: !res.data.admin ? `/usermenu` : "/adminmenu"}) 
      },2000) 
    })
    .catch((error) =>{
      console.log(error)
    })
  }

  handleSubmitIn = (e) => {
    e.preventDefault()
    let email=e.target.elements.email.value;
    let password=e.target.elements.password.value;
    axios.post ("http://localhost:2000/user/login", {email, password} )
    .then ((res)=>{
      if( !res.data.ok ) return alert(res.data.message)
      localStorage.setItem("token",JSON.stringify(res.data.token))
      setTimeout( () => {
        this.props.checkLog(res.data.admin);
        this.props.history.push({pathname: !res.data.admin ? `/usermenu` : "/adminmenu"}) 
      },1000) 
    })
    .catch ((error)=>{
      console.log(error)
    })
  }

  handlePassword = (e) => {
    let pass = e.target.value
    let securityCheck = 0;
    if (pass.length > 6) {this.setState({length:true})}
      else this.setState({length:true})
    if (/[A-Z]/.test(pass) === true) { securityCheck++ }
    if (/[a-z]/.test(pass) === true) { securityCheck++ }
    if (securityCheck === 2 && this.state.length===true) {
      this.setState({ color: "green", secure: "Your password is secure.", password: pass }) }
      else (
      this.setState({ color: "red", secure: "Your password is not secure.", password: pass }))
    if (pass === this.state.passwordMatch) {
      this.setState({ color02: "green", match: "Your password matches.",password: pass })}
      else 
        this.setState({ color02: "red", match: "Your passwords dont match.",password: pass })
  }

  handlePasswordConfirm = (e) => {
    let passconfirm = e.target.value;
    if (this.state.password === passconfirm) {
      this.setState({ color02: "green", match: "Your password matches.", passwordMatch: passconfirm })
    }
    else {
      this.setState({ color02: "red", match: "Your passwords dont match.", passwordMatch: passconfirm })
    }
  }

  
  render() {

    return (
      <div
        style={{
          height: "80vh",
          background: "white",
          color: "black",
          alignItems: "left",
          margin: "0",
        }}>

        <Columns columns="4">
          <h2>SIGN IN HERE </h2>
          <form onSubmit={this.handleSubmitIn}>

            Email:<p></p> <input name="email" /> <p></p>
            Password:<p></p> <input type="password" name="password" /> <p></p>
            <button>SUBMIT INFO</button><p></p>
            <span style={{ fontSize: "8px" }}>Forgot your Password? Click here</span>
          </form>
          <div>
            <h2>SIGN UP HERE</h2>
            <h5>Minimum length 6 chars.</h5>
            <h5 style={{ color: this.state.color02 }}>{this.state.match}</h5>
            <h5 style={{ color: this.state.color }}>{this.state.secure}</h5>
          </div>
          <form onSubmit={this.handleSubmitUp}>

            Email: <input name="email" /><p></p>
            Name: <input name="name" /><p></p>
            Last Name: <input name="lastName" /><p></p>
            Adress: <input name="adress" /><p></p>
            Postal Code <input name="CP" /><p></p>
            City <input name="city" /><p></p>
            Province <input name="province" /><p></p>
            State <input name="state" /><p></p>
            Password: <input type="password" name="password" onChange={this.handlePassword} /><p></p>
            Confirm Password: <input type="password" name="passwordConfirm" onChange={this.handlePasswordConfirm} /><p></p>
            <button>SUBMIT INFO</button>
          </form>
        </Columns>
      </div>
    )
  }
}

export default Sign;