import React from "react";

class Thanks extends React.Component {

  render() {
 
    return <div style={{
      height: "80vh",
      background: "white",
      display: "flex",
      alignItems: "enter",
      justifyContent: "space-around",
      margin: "0",
      listStyle: "none"
    }}> Thanks for shopping with us </div>;
  
}
}
export default Thanks;