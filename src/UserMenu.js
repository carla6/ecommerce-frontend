import React from "react";
import axios from "axios"

class UserMenu extends React.Component {

  componentWillMount() {
    const token = JSON.parse(localStorage.getItem("token"))
    if (token === "undefined") return null
    axios.get(`http://localhost:2000/user/verifyToken/${token}`)
      .then((res) => {
        if (!res.data.ok) {
          this.props.history.push("/sign")
          alert("Wrong username or password")
        }
      })
      .catch((error) => {
        console.log(error)
      })
  }

  logOut = () => {
    this.props.checkLog(false);
    localStorage.clear();
    this.props.history.push({ pathname: "/" })
  }
  render() {
    return <div style={{
      background: "white",height: "80vh",
      margin: "0",
    }}><h3>User Menu</h3>
      <button >Check Wishlist</button>
      <button onClick={this.logOut}>Log out</button>
      <form></form>
      </div>;
  }
}

export default UserMenu;