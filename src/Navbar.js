import React from "react";
import { NavLink, withRouter } from "react-router-dom";

class NavBarComp extends React.Component {

  handleSearch = (e) => {
    e.preventDefault()
    var search = e.target.firstElementChild.value;
    this.props.searchEngine(search)
    this.props.history.push({ pathname: `/search/${search}` })
  }

  render() {
    return (
      <ul
        style={{
          height: "10vh",
          background: "black",
          color: "white",
          display: "flex",
          alignItems: "center",
          justifyContent: "space-around",
          margin: "0",
          listStyle: "none"
        }}>
        <li>
          <NavLink exact to="/" activeStyle={activeStyle}>
            Home
          </NavLink>
        </li>
        <li>
          <NavLink exact to="/news" activeStyle={activeStyle}>
            News
          </NavLink>
        </li>
        <li>
          <NavLink exact to="/shop" activeStyle={activeStyle}>
            Shop
          </NavLink>
        </li>
        <li>
          <NavLink exact to="/about" activeStyle={activeStyle}>
            About us
          </NavLink>
        </li>
        <li>
          {this.props.isLogged && this.props.admin === true ?
            <NavLink exact to="/adminmenu" activeStyle={activeStyle}>Admin Menu</NavLink> : null}
          {this.props.isLogged && this.props.admin === false ?
            <NavLink exact to="/usermenu" activeStyle={activeStyle}> User Menu </NavLink> : null}
          {!this.props.isLogged ? <NavLink exact to="/sign" activeStyle={activeStyle}> Sign In/Up </NavLink> : null}
        </li>

        <li>
          <form onSubmit={this.handleSearch} >
            <input placeholder="Search" />
          </form>
        </li>
      </ul>
    );
  }
}
let activeStyle = {
  color: "white"
};
export default withRouter(NavBarComp)