import React from "react";
import AddItem from "./AddItem";
import axios from "axios";


class AdminMenu extends React.Component {

 state={menu: ""}

  componentWillMount() {
    const token = JSON.parse(localStorage.getItem("token"))
    axios.get(`http://localhost:2000/user/verifyToken/${token}`)
      .then((res) => {
        if (!res.data.admin) {
          this.props.history.push("/sign")
          alert("Wrong credentials")
        }
      })
      .catch((error) => {
        console.log(error)
      })
  }

  logOut = () => {
    this.props.checkLog(false);
    localStorage.removeItem("token")
    this.props.history.push({ pathname: "/" })
  }
  
addClick=()=>{this.setState({menu: "add"});}

render() {
    return (
    <div style={{
      height: "80vh",
      background: "white",
      alignItems: "center",
      justifyContent: "space-around",
    }}>
      <button onClick={this.addClick}>Add product</button>
      <button onClick={this.logOut}>Log Out</button>
      {this.state.menu==="add"? <AddItem/>:null}
  
    </div>
   
    )
  
  }
}

export default AdminMenu;

