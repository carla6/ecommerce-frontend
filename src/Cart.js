import React from "react";
import DisplayCart from "./DisplayCart";
import { withRouter } from "react-router-dom";

class Cart extends React.Component {

  goToCheckout=()=>{
    this.props.hideCart();
    this.props.history.push("/confirm");
  }

  render() {
    return (
    
      <div style={{
        gridTemplateColumns: "1fr 1fr 1fr"}}>
        {this.props.cartContent.length > 0 ?
          this.props.cartContent.map((ele) => {
              return <DisplayCart product={ele}
                                  rout={this.props.history}
                                  hideCart={this.props.hideCart}
                                  deleteFromCart={this.props.deleteFromCart}
                                  addFunction={this.props.addFunction}
                                  moveToWish={this.props.moveToWish}
                                  substractFunction={this.props.substractFunction}/>
            })
        : "Your cart is empty.What are you waiting for? We have awesome comics."}
        <h1>{this.props.total}EUR</h1>
        <button onClick={this.goToCheckout}>CONFIRM YOUR ORDER</button>
        <button onClick={this.props.hideCart}>HIDE CART</button>
      </div>)
  }
}

export default withRouter(Cart)