import React from "react";
import DisplaySearch from "./DisplaySearch"

class UserSearch extends React.Component {

  componentWillMount(){

    this.props.searchEngine(this.props.match.params.userSearch)
  }

  render() {
    let foundProducts = this.props.foundProducts || []
    return (  
    <div style={{display:"grid",
                gridTemplateColumns:"1fr 1fr 1fr"
    }}>
   { foundProducts.length>0 ?
    foundProducts.map((ele) => 
    { 
      return <DisplaySearch product={ele}
                           rout={this.props.rout}
                           admin={this.props.admin}
                           deleteFunction={this.props.deleteFunction}
                           detailsFunction={this.props.detailsFunction}
                           addToWishFunction={this.props.addToWishFunction}
                           addFunction={this.props.addFunction}/> })
    : "No matches Found"
  }
     </div>
    )
      
}
}


export default UserSearch;