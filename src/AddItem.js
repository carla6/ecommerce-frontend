import React from "react";
import axios from "axios";
import UploadImages from "./UploadImages"

class AddItem extends React.Component {
     
    state={ coverURL:"", id:""};

       addProduct = (e) => {
        e.preventDefault()
        let id = e.target.elements.id.value.toLowerCase();
        let product = e.target.elements.product.value.toLowerCase();
        let title = e.target.elements.title.value.toLowerCase();
        let price = e.target.elements.price.value.toLowerCase();
        let number = e.target.elements.number.value.toLowerCase();
        let format = e.target.elements.format.value.toLowerCase();
        let published = e.target.elements.published.value.toLowerCase();
        let editorial = e.target.elements.editorial.value.toLowerCase();
        let distributor = e.target.elements.distributor.value.toLowerCase();
        let writer = e.target.elements.writer.value.toLowerCase();
        let drawer = e.target.elements.drawer.value.toLowerCase();
        let pages = e.target.elements.pages.value.toLowerCase();
        let description = e.target.elements.description.value.toLowerCase();
        let cover = e.target.elements.cover.value.toLowerCase();
        let stock = e.target.elements.stock.value.toLowerCase();
        let hidden = document.getElementById("hidden").checked;
        let sale= document.getElementById("onSale").checked;
        let salePrice = e.target.elements.salePrice.value.toLowerCase();
        let newProduct = {
            id, product, title, price, number, format, published, editorial, writer, drawer, pages, description, cover,
            distributor, stock,sale, salePrice, hidden};   
        axios.post("http://localhost:2000/admin/add", newProduct)
            .then((res) => {
                alert(res.data.message)
                document.getElementById("newProduct").reset()
            })
            .catch((error) => {
                console.log(error)
            })
    }

    handleChange=(e)=>{
       this.setState({id:e.target.value});
    }

    render() {
        return (
            <div>
                <form id="newProduct" onSubmit={this.addProduct} >
                    Item ID:<input onChange={this.handleChange} name="id" />
                    Product:<input name="product" />
                    Title:<input name="title" />
                    Price:<input name="price" />
                    Number:<input name="number" />
                    Format:<input name="format" />
                    Published:<input name="published" />
                    Editorial:<input name="editorial" />
                    Writer:<input name="writer" />
                    Drawer:<input name="drawer" />
                    Pages:<input name="pages" />
                    Description: <input name="description" />
                    Cover:  <input name="cover" />
                    Distributor: <input name="distributor" />
                    Stock: <input type="number" name="stock" />
                    On Sale: <input type="checkbox" id="onSale" />
                    Sale Price: <input name="salePrice" />
                    Hide Item: <input type="checkbox" id="hidden" />
                    <button >Submit New Item</button>
                </form>
                <UploadImages productId={this.state.id} setURL={this.setURL}/>
            </div>
        )

    }
}

export default AddItem;