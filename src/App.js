import axios from "axios";
import About from "./About";
import AdminMenu from "./AdminMenu";
import Cart from "./Cart";
import ContactForm from "./ContactForm";
import Thanks from "./Thanks";
import Confirm from "./Confirm";
import Home from "./Home";
import Navbar from "./Navbar";
import News from "./News";
import Product from "./Product"
import Sign from "./Sign";
import Shop from "./Shop";
import Search from "./Search";
import UserMenu from "./UserMenu";
import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";


export default class App extends React.Component {

  state = { admin: false, isLogged: false, displayCart: false, cart: [], dateDisplay:[], soldDisplay:[] }

  componentWillMount() {
    const token = JSON.parse(localStorage.getItem("token"));
    axios.get(`http://localhost:2000/user/verifyToken/${token}`)
      .then(res => {
        if (res.data.ok) { this.setState({ isLogged: true }) }
        if (res.data.admin) { this.setState({ admin: true }) }
      })
      .catch(e => console.log(e))
    const cart = JSON.parse(localStorage.getItem("cart"));
    if (cart === null) { localStorage.setItem("cart", JSON.stringify([])) };
    this.setState({ cart });
    this.calculateTotal()
  }

  addToCart = (id) => {
    const cart = JSON.parse(localStorage.getItem("cart"))
    const index = cart.findIndex((ele) => ele.id === id)
    index !== -1 ? (cart[index].qty += 1)
      : cart.push({ id, qty: 1 })
    localStorage.setItem("cart", JSON.stringify(cart))
    this.setState({ cart })
    this.calculateTotal()
  }

  addToWish = (id) => {
    let productId = id;
    let userId = JSON.parse(localStorage.getItem("token"));
    userId != null ? (
      axios.post("http://localhost:2000/user/addtowish", { productId, userId })
        .then((res) => {
          alert(res.data.message)
        })
        .catch((error) => {
          console.log(error)
        }))
      : alert("You must be signed in to add items to your wishlist")
  }

  calculateTotal = () => {
    let cart = JSON.parse(localStorage.getItem("cart"))
    axios.post(`http://localhost:2000/admin/findAmount`, { cart })
      .then(res => {
        let total = 0
        res.data.origArray.forEach(element => {
          total += (element.qty * element.price);
        });
        this.setState({ total })
      })
      .catch(e => console.log(e))
  }


  checkLog = (e) => {
    this.setState({ isLogged: !this.state.isLogged });
    if (e === true) { this.setState({ admin: true }) } else this.setState({ admin: false })
  }

  checkStock= ()=>{
    let products=this.state.cart;
    axios.post ("http://localhost:2000/admin/checkStock", {products})
  .then ((res)=>{
    res.data.forEach(element => {
      if (element.qty>Number(element.stock)) {
        element.qty=Number(element.stock)
        element.modified= true
      }
      else element.modified= false
    });

    localStorage.setItem("cart", JSON.stringify(res.data))
    this.setState({cart:res.data});
   
    this.calculateTotal()
  })
 .catch ((error)=>{
   console.log(error)
 }) 
}

clearCart= ()=>{

  this.setState({cart:[], total:0})
  localStorage.removeItem("cart")
  localStorage.setItem("cart", JSON.stringify([]))

}

  deleteFromCart = (id) => {
    const cart = JSON.parse(localStorage.getItem("cart"))
    const index = cart.findIndex((ele) => ele.id === id)
    cart.splice(index,1)
    localStorage.setItem("cart", JSON.stringify(cart))
    this.setState({ cart })
    this.calculateTotal()
  }

  deleteItem = (id) => {
    let conf = window.confirm("Item will be deleted. Confirm?");
    if (conf === true) (
      axios.post('http://localhost:2000/admin/remove', { id })
        .then((res) => {
          alert(res.data.message)
          this.setState({ display: "none" })
        })
        .catch((error) => {
          alert(error)

        }))
  }

  hideCart = () => {
    this.setState({ displayCart: false })
  }

  moveToWish = (id) => {
    this.addToWish(id)
    const cart = JSON.parse(localStorage.getItem("cart"))
    const index = cart.findIndex((ele) => ele.id === id)
    cart.splice(index,1)
    localStorage.setItem("cart", JSON.stringify(cart))
    this.setState({ cart })
    this.calculateTotal()
  }

  substractFromCart = (id) => {
    const cart = JSON.parse(localStorage.getItem("cart"))
    const index = cart.findIndex((ele) => ele.id === id)
    cart[index].qty -= 1
    if (cart[index].qty === 0) return this.deleteFromCart()
    localStorage.setItem("cart", JSON.stringify(cart))
    this.setState({ cart })
    this.calculateTotal()

  }

  searchEngine = (search) => {

    axios.get(`http://localhost:2000/admin/${search}`)
      .then((res) => {
        this.setState({ foundProducts: res.data })
      })

      .catch((error) => {
        console.log(error)
      })
  }

  searchByDate = (search) => {

    axios.get(`http://localhost:2000/admin/date/`)
      .then((res) => {
        this.setState({ dateDisplay: res.data })
      })
      .catch((error) => {
        console.log(error)
      })
  }

  searchBySold = (search) => {

    axios.get(`http://localhost:2000/admin/sold/`)
      .then((res) => {
        this.setState({ soldDisplay: res.data })
      })
      .catch((error) => {
        console.log(error)
      })
  }

  showCart = () => {

    this.setState({ displayCart: true })
    this.setState({ showOther: false })
  }

  render() {
    return (

      <Router>
        <div>
            {this.state.displayCart === false ?
              <div>
                <Navbar isLogged={this.state.isLogged} admin={this.state.admin} searchEngine={this.searchEngine} />
                <Route exact path="/" component={Home} />
                <Route path="/contact" component={ContactForm}/>
                <Route path="/about" component={About} />
                <Route path="/shop" render={(props)=><Shop search={this.state.dateDisplay} sold={this.state.soldDisplay} searchBySold={this.searchBySold} searchByDate={this.searchByDate}/>} />
                <Route path="/thanks" component={Thanks}/>
                <Route path="/news" component={News} />
                <Route path="/sign" render={(props) => <Sign
                  {...props}
                  checkLog={this.checkLog} />} />
                <Route path="/cart" component={Cart} />
                <Route exact path="/usermenu" render={
                  (props) => <UserMenu {...props} checkLog={this.checkLog} />} />
                <Route exact path="/adminmenu" render={
                  (props) => <AdminMenu {...props} checkLog={this.checkLog} />} />
                <Route exact path="/productdetails/:product" render={(props) => <Product
                  {...props}
                  admin={this.state.admin}
                  addToCart={this.addToCart}
                  addToWish={this.addToWish}
                  rout={props.history} />} />
                <Route path="/search/:userSearch" render={
                  (props) => <Search addFunction={this.addToCart}
                    {...props}
                    deleteFunction={this.deleteItem}
                    detailsFunction={this.details}
                    addToWishFunction={this.addToWish}
                    searchEngine={this.searchEngine}
                    foundProducts={this.state.foundProducts}
                    rout={props.history}
                    admin={this.state.admin} />} />
                <Route path="/confirm" render={(props) => <Confirm checkStock={this.checkStock} calculateTotal={this.calculateTotal}
                  cartContent={this.state.cart}
                  showCart={this.showCart}
                  clearCart={this.clearCart}
                  total={this.state.total}
                  rout={props.history} />} />
                 <button onClick={this.showCart}>SHOW CART ({this.state.total}EUR)</button>  </div>
              :<div style={{ display:"flex", justifyContent:"right"}}>
               <Cart
                cartContent={this.state.cart}
                addFunction={this.addToCart}
                moveToWish={this.moveToWish}
                hideCart={this.hideCart}
                deleteFromCart={this.deleteFromCart}
                total={this.state.total}
                display={this.state.display}
                substractFunction={this.substractFromCart} /> </div>
                }
            
        </div>
      </Router>
    );
  }
}
