import React from "react";
import { withRouter } from "react-router-dom";
import Checkout from "./Checkout"


class Confirm extends React.Component {

  state={displayPayment:false, updatedCart:[]}

componentWillMount(){ 
 this.props.checkStock(this.props.cartContent)

}

  goBack = () => {
    this.props.rout.push("/shop")
    this.props.showCart()
}

byeBye=(e)=>{

if (e==="success"){ this.props.rout.push("/thanks");
this.props.clearCart()}
}

handleSubmit=(e)=>{
  e.preventDefault()
  let email = e.target.elements.email.value;
  let name = e.target.elements.name.value;
  let lastName = e.target.elements.lastName.value;
  let adress = e.target.elements.adress.value;
  let cp = e.target.elements.CP.value;
  let city = e.target.elements.city.value;
  let province = e.target.elements.province.value;
  let state = e.target.elements.state.value;
  let client = { email, name, lastName, adress, cp, city, province, state};
 this.setState ({displayPayment:true}, ()=>{
   this.setState ({client:client})
 });

}

  render() {
    return (  
      <div>  CONFIRM YOUR ORDER
        
        <form style={{ fontSize: "8px" }}
            onSubmit={this.handleSubmit}>

            Email: <input name="email" required /><p></p>
            Name: <input name="name" required /><p></p>
            Last Name: <input name="lastName" required /><p></p>
            Adress: <input name="adress" required /><p></p>
            Postal Code <input name="CP" required/><p></p>
            City <input name="city"required /><p></p>
            Province <input name="province" required/><p></p>
            State <input name="state" required /><p></p>
            <button>Submit info</button>
            </form>
        <div></div>
           
           <div>{this.props.total} EUR</div>
           <button onClick={()=>{this.goBack()}}>Go Back</button>
           {this.state.displayPayment? <Checkout byeBye={this.byeBye} cartContent={this.props.cartContent}
            amount={this.props.total} client={this.state.client} />:null }
      </div>
      )
  }
}

export default withRouter(Confirm)