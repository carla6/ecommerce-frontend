import React from "react";

class Home extends React.Component {
  render() {
    return <div style={{
      height: "80vh",
      background: "white",
      display: "flex",
      alignItems: "center",
      justifyContent: "space-around",
      margin: "0",
      listStyle: "none"
    }}>I am Home</div>;
  }
}

export default Home;
