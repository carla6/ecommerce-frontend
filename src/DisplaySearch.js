import React from "react";


class DisplaySearch extends React.Component {

    state = { display: "inline" }

    details=(product)=>{

    this.props.rout.push({pathname: `/productdetails/${this.props.product.id}`,state:{product: this.props.product}});
    }

    render() {
        return (
            <div style={{
                width: "100%",
                border: "1px solid black",
                display: this.state.display
            }}>
                <p>id: {this.props.product.id}</p>
                <p>title: {this.props.product.title}</p>
                <p>price: {this.props.product.price}</p>
                <button onClick= {() => this.details(this.props.product)}
                > See Details</button>
                 {this.props.admin === true ? [
                 <button onClick={() => this.props.deleteFunction(this.props.product.id)}>Delete Item</button>]:null}
                 {this.props.admin === false & this.props.product.stock>0 ?
                <button onClick={() => this.props.addFunction(this.props.product.id)}>Add to cart</button>: <p>Not available</p>}
                {this.props.admin === false ?<button onClick={() => this.props.addToWishFunction(this.props.product.id)}>Add to Wishlist</button>
                : null}
                {this.props.hidden===true? this.setState({diplay:"none"}):null}

            </div>
        )
                 }
}

export default DisplaySearch;