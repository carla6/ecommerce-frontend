import React from "react";
import axios from "axios"

class DisplayCart extends React.Component {

 state = { id: this.props.product.id, qty: this.props.product.qty }

    componentWillMount() {

        axios.get(`http://localhost:2000/admin/${this.state.id}`)
            .then((res) => {

                this.setState({ product: res.data[0] })
            })

            .catch((error) => {
                console.log(error)
            })
    }

    details = (product) => {
        this.props.hideCart()
        this.props.rout.push({ pathname: `/productdetails/${this.state.id}`, state: { product: this.state.product } });
    }

    render() {
        return (
            <div style={{
                width: "100%",
                border: "1px solid black",
                display: "block"
            }}>
                <p>id:</p>{this.props.product.id}
                <p>qty:</p>{this.props.product.qty}
                <button onClick={() => this.details(this.state.product)}> See Details</button>
                <button onClick={()=>this.props.addFunction(this.state.product.id)}>Add</button>
                <button onClick={()=>this.props.substractFunction(this.state.product.id)}> Substract</button>
                <button onClick={()=>this.props.deleteFromCart(this.state.product.id)}>Remove from Cart</button>
                <button onClick={()=>this.props.moveToWish(this.state.product.id)}> Move to Wishlist</button>
            </div>
        )
    }
}

export default DisplayCart;