import React from "react"



class ProductDetails extends React.Component {

    state={product:[]}

    goBack = () => {
        this.props.rout.goBack()
    }

    render() {
        console.log(this.props)
        return (<div>
            <h4>Product Details </h4>
                <p>ID: {this.props.rout.location.state.product.id} </p>
            <p>TITLE: {this.props.rout.location.state.product.title}</p>
            <button onClick={()=>this.props.addToCart(this.props.rout.location.state.product.id)}>Add to Cart</button>
            <button onClick={()=>this.props.addToWish(this.props.rout.location.state.product.id)}>Add to WishList</button>
            <button onClick={() => this.goBack()}> Go Back</button>

        </div>
        )
    }
}

export default ProductDetails;