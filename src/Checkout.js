import React from 'react'
import axios from 'axios';

// import stripe checkout first
import StripeCheckout from 'react-stripe-checkout';


// that's your publishable jey obtained form strip, for testing use testing key
const STRIPE_PUBLISHABLE = 'pk_test_ZZtKuyOWta2zp6J2l91Ea1oX00mUgiuJnh';

// this will be the route to hit on the server after getting token from stripe
const PAYMENT_SERVER_URL = ' http://localhost:2000/orders/purchase';


const CURRENCY = 'EUR';

const fromEuroToCent = amount => amount * 100;

const successPayment = data => {
	alert('Payment Successful');
	createOrder()
};

const errorPayment = data => {
	alert('Payment Error');
	console.log(data);
};

const createOrder = ()=>{

	axios.post ("http://localhost:2000/orders/create", {userDetails,total, orderDetails})
	.then ( res =>{
	  bye("success")
	})
	.catch(error =>{
		console.log(error)
		bye("error")
	})
	
};


const onToken = (amount, description) => token =>
axios.post(PAYMENT_SERVER_URL,
{
	description,
	source: token.id,
	currency: CURRENCY,
	amount: fromEuroToCent(amount)
})
.then(successPayment)
.catch(errorPayment);

var userDetails, total, orderDetails, bye ;

const Checkout = ({ byeBye, name, description, amount, label, client, cartContent}) =>{
console.log(cartContent, "CART CONTENT");
bye = byeBye
userDetails=client
total=amount
orderDetails = cartContent
return  <StripeCheckout
name={"Your Order"}
description={"Xolskis Comic Shop"}
amount={fromEuroToCent(amount)}
token={onToken(amount, description)}
currency={CURRENCY}
stripeKey={STRIPE_PUBLISHABLE}
label = {"Payment"}
/>}

export default Checkout;
