import React from "react";

class UploadImages extends React.Component {

	uploadWidget = () => {
        window.cloudinary.openUploadWidget({ 
        	cloud_name: 'dq1cd9zmq', 
        	upload_preset: 'xs3qs7h6', 
			tags:['user'],
			// stylesheet:widgetStyle
        },
            (error, result)=> {
			result.forEach(result => {	
		               if(error){
					//debugger
                }else{
					fetch('http://localhost:2000/images/upload', {
						method: 'POST',
						headers: {
						  Accept: 'application/json',
						  'Content-Type': 'application/json'
						},
						body: JSON.stringify({
						  URL:result.secure_url, 
                          publicId:result.public_id,
                          productId: this.props.productId
						}),
						}).then((response) => response.json())
							.then((responseJson) => {
								console.log(responseJson)
						}).catch((e)=>{
							debugger
						})
							  
				}
			});
            });
    }


    render(){


        return(

            <div className="flex_upload">
                <div className="upload">
					<button className ="button_small"
                    	onClick={this.uploadWidget} > Upload Images
                    </button>
                </div>
            </div>
        )
    }
}


export default UploadImages;